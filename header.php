<?php /******************************************
  The start of the document
*********************************************/ ?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <?php require_once('html-head.php') ; ?>
</head>
<body <?php body_class(); ?>>

  <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">

      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand" href="#"><?php bloginfo( 'title' ) ; ?></a>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item xactive">
            <a class="nav-link" href="/">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="/about" id="dropdown-events" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="/about">About Nerf Please</a>
              <a class="dropdown-item" href="/about/join">Join The Clan</a>
              <a class="dropdown-item" href="/about/membership-requirements">Membership Requirements</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="/events" id="dropdown-events" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Events</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="/events">View All</a>
              <a class="dropdown-item" href="/create-an-event">Create An Event</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/members">Members</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/news">Blog</a>
          </li>
        </ul>
      </div>
    </nav>

  <div class="container">

    <h1><?php the_title(); ?></h1>
