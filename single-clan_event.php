<?php /******************************************
File Description
*********************************************/ ?>


<?php get_header(); ?>
<?php
  $event = pods( 'clan_event', get_the_ID() );
  $author_gamertag = get_the_author_meta( 'clan_member_xbox_gamertag', $user_ID );
  $author_id = get_the_author_meta('ID');
?>

<?php
  $args = array(
    'orderby'       => 'name',
    'order'         => 'ASC',
    // 'number'        => null,
    // 'optioncount'   => false,
    // 'exclude_admin' => true,
    // 'show_fullname' => false,
    // 'hide_empty'    => true,
    // 'echo'          => true,
    // 'feed'          => [empty string],
    // 'feed_image'    => [empty string],
    // 'feed_type'     => [empty string],
    // 'style'         => 'list',
    'html'          => true,
    // 'exclude'       => [empty string],
    // 'include'       => [empty string]
  );
  wp_list_authors( $args );
?>
<article id="post-<?php the_ID(); ?>" class="<?php post_class(); ?>">
  <div class="row">
    <div class="col-lg-6 pb-4">
      <img class="img-fluid" src="<?php // echo get_clan_event_img_url(); ?>"">
    </div>
    <div class="col-lg-6 pb-4">
      <table class="table table-hover table-sm">
        <tbody>
          <tr>
            <th>Date</th>
            <td><?php echo pretty_event_date(); ?></td>
          </tr>
          <tr>
            <th>Time</th>
            <td><?php echo pretty_event_Time(); ?></td>
          </tr>
          <tr>
            <th>Host</th>
            <td>
              <a href="<?php echo get_author_posts_url( $author_id, $author_gamertag ); ?>">
                <?php the_author_meta( 'clan_member_xbox_gamertag', $user_ID ); ?>
              </a>
            </td>
          </tr>
          <tr>
            <th>Type</th>
            <td><?php echo $event->field('clan_event_category'); ?> &bull; <?php echo $event->field('clan_event_sub_category'); ?></td>
          </tr>
          <tr>
            <th>Style</th>
            <td><?php echo $event->field('clan_event_play_style'); ?></td>
          </tr>
          <tr>
            <th>Attendees</th>
            <td>list of people who signed up</td>
          </tr>
          <tr>
            <th>Tags</th>
            <td><?php the_tags(''); ?></td>
          </tr>
          <tr>
            <th>Are you attending?</th>
            <td>
              <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-sm btn-secondary">Yes</button>
                <button type="button" class="btn btn-sm btn-secondary">Maybe</button>
                <button type="button" class="btn btn-sm btn-secondary">No</button>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div>
    <?php echo $event->display('clan_event_description'); ?>
  </div>

  <hr>
  <p>FILE: single-clan-event.php</p>
</article>



<?php get_footer(); ?>
