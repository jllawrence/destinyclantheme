<?php /******************************************
This file outputs the content for the create an
event page. No joke.
*********************************************/ ?>


<?php get_header(); ?>


<?php the_content(); ?>

<div id="form-create-an-event" >
  <?php echo do_shortcode('[pods-form name="clan_event" fields="clan_event_title, clan_event_start_date, clan_event_start_time, clan_event_play_style, clan_event_category, clan_event_sub_category, clan_event_description" label="Create Event"]'); ?>
</div>

<hr>
<p>FILE: page-create-an-event.php</p>



<?php get_footer(); ?>
