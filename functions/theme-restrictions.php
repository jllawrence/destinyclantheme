<?php /******************************************

************************************************/


// No file editing from admin
define( 'DISALLOW_FILE_EDIT', true );

// No WP core auto updates. Updates must be made manually.
define( 'WP_AUTO_UPDATE_CORE', false );

// Hide the pods shortcode button from the post editor
add_filter( 'pods_admin_media_button', '__return_false' );
