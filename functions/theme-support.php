<?php /******************************************
Functions for theme support, register sidebars,
and register navs
************************************************/


// Add theme support
add_theme_support( 'menus' );
add_theme_support( 'title-tag' );

// Register Menus
register_nav_menus(
  array(
    'menu-top'    => __( 'Top Menu', 'destinyclantheme' ),
  )
);
