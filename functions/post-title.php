<?php /******************************************
Functions to output the correct post title
************************************************/


function get_clan_post_title() {
  $result = '';
  if ( get_post_type() == 'clan_event' ) {
    $event = pods( 'clan_event', get_the_ID() );
    $result = $event->field('clan_event_title');
  }
  return $result;
}
