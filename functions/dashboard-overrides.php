<?php /******************************************

************************************************/


// Remove the wp welcome panel
remove_action('welcome_panel', 'wp_welcome_panel');


// Remove the WP logo from dashboard
function remove_wp_logo( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'wp-logo' );
}
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );


// Remove some WP Menu Pages
function remove_wp_menu_pages() {
    // remove_menu_page('themes.php');
    remove_menu_page('customize.php');
    remove_menu_page('tools.php');
    remove_menu_page('import.php');
    remove_menu_page('export.php');
}
add_action( 'admin_menu', 'remove_wp_menu_pages' );


// Remove WP Dashboard Panels
function remove_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );


// Remove default WP footer thank you text
function modify_admin_footer_text($footer_text =''){
  $result = '&copy; ' . date('Y') . ' ' . get_bloginfo( 'title' );
  return $result;
}
add_filter('admin_footer_text', 'modify_admin_footer_text', 1000);


// Remove default WP footer version
function modify_admin_footer_version($footer_text =''){
    return '';
}
add_filter('update_footer', 'modify_admin_footer_version', 1000);


// Remove unnecessary options from User Settings panel
// remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
function remove_user_options(){
  echo '
    <script type="text/javascript">jQuery(document).ready(function($) {
      // $(\'form#your-profile > h2:first\').remove(); // remove the "Personal Options" title
      $(\'form#your-profile > h2\').remove(); // remove the all default WP section headings
      $(\'form#your-profile > h3\').remove(); // remove the all default Pods section headings
      $(\'form#your-profile tr.user-rich-editing-wrap\').remove(); // remove the "Visual Editor" field
      $(\'form#your-profile tr.user-admin-color-wrap\').remove(); // remove the "Admin Color Scheme" field
      $(\'form#your-profile tr.user-comment-shortcuts-wrap\').remove(); // remove the "Keyboard Shortcuts" field
      $(\'form#your-profile tr.user-admin-bar-front-wrap\').remove(); // remove the "Toolbar" field
      $(\'form#your-profile tr.user-language-wrap\').remove(); // remove the "Language" field
      $(\'form#your-profile tr.user-first-name-wrap\').remove(); // remove the "First Name" field
      $(\'form#your-profile tr.user-last-name-wrap\').remove(); // remove the "Last Name" field
      $(\'form#your-profile tr.user-nickname-wrap\').hide(); // Hide the "nickname" field
      $(\'table.form-table tr.user-display-name-wrap\').remove(); // remove the “Display name publicly as” field
      $(\'table.form-table tr.user-url-wrap\').remove();// remove the "Website" field in the "Contact Info" section
      $(\'h2:contains("About Yourself"), h2:contains("About the user")\').remove(); // remove the "About Yourself" and "About the user" titles
      $(\'form#your-profile tr.user-description-wrap\').remove(); // remove the "Biographical Info" field
      $(\'form#your-profile tr.user-profile-picture\').remove(); // remove the "Profile Picture" field
    });</script>
    <style>
      .pods-form-ui-field-name-pods-meta-clan-member-bio #wp-pods-form-ui-pods-meta-clan-member-bio-editor-tools {
        display: none !important; // Hide
      }
    </style>
  ';
}
add_action('admin_head','remove_user_options');
