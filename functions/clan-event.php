<?php /******************************************
Functions for Events CPT created with Pods
************************************************/


// clan_event_title
// clan_event_start_date
// clan_event_start_time
// clan_event_play_style
// clan_event_category
// clan_event_sub_category
// clan_event_description
// clan_event_attendees

// Change the clan_event More Fields metabox label in admin
function my_custom_title( $title, $pod, $fields, $pod_type, $pod ) {
  $title = 'Event Details';
  $pod = 'clan_event';
  // $fields = '';
  // $pod_type = '';
  // $pod = '';
  return $title;
}
add_filter( 'pods_meta_default_box_title', 'my_custom_title', 10, 5 );


// Setup columns when viewing events in dashboard
function setup_event_columns($columns) {
    return array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Title'),
        'clan_event_game_type' => __('Game Type'),
        'clan_event_host' => __('Host'),
        'clan_event_start_date_time'=>  __('Event Date/Time'),
    );
}
add_filter('manage_clan_event_posts_columns' , 'setup_event_columns');


// Show values for event columns
function setup_event_column_values( $column, $post_id ) {
  switch ( $column ) {
    case 'clan_event_play_style' :
      echo get_post_meta( $post_id , 'clan_event_play_style' , true );
      break;
    case 'clan_event_game_type' :
      echo get_post_meta( $post_id , 'clan_event_category' , true );
      echo ' / ';
      echo get_post_meta( $post_id , 'clan_event_sub_category' , true );
      echo ' / (';
      echo get_post_meta( $post_id , 'clan_event_play_style' , true );
      echo ')';
      break;
    case 'clan_event_host' :
      echo get_the_author_meta( 'clan_member_xbox_gamertag' , get_the_author_meta('ID') );
      break;
    case 'clan_event_start_date_time' :
      echo get_post_meta( $post_id , 'clan_event_start_date' , true );
      echo ' ';
      echo get_post_meta( $post_id , 'clan_event_start_time' , true );
      break;
  }
}
add_action( 'manage_clan_event_posts_custom_column' , 'setup_event_column_values', 10, 2 );


// Set the post meta event title as the post title
function modify_event_post_title($title, $id) {
  if ('clan_event' == get_post_type($id)) {
      return get_post_meta( $id, 'clan_event_title', true );
   }
  else {
      return $title;
  }
}
add_filter('the_title', 'modify_event_post_title',10, 2);


// Format event date and time
function pretty_event_date() {
  $event = pods( 'clan_event', get_the_ID() );
  $date = $event->field('clan_event_start_date');
  $date = date_create($date);
  return date_format($date, 'l, F Y');
}
function pretty_event_time() {
  $event = pods( 'clan_event', get_the_ID() );
  $time = $event->field('clan_event_start_time');
  $time = date_create($time);
  return date_format($time, 'h:i a');
}
function pretty_event_date_time() {
  $event = pods( 'clan_event', get_the_ID() );
  $date = $event->field('clan_event_start_date');
  $date = date_create($date);
  $time = $event->field('clan_event_start_time');
  $time = date_create($time);
  return date_format($date, 'l, F Y') . date_format($time, 'h:i a');
}

function get_clan_event_img_url() {
    $event = pods( 'clan_event', get_the_ID() );
    $img_obj = $event->field('clan_event_image');
    $img_url = $img_obj['guid'];
    $result = $img_url;
    return $result;
}


// Remove the 'Text' editor and show WYSIWYG only
function hide_editor_tabs_for_event_description() {
  echo '
    <style>
      .pods-form-ui-field-name-pods-meta-clan-event-description .wp-editor-tabs {
        display: none !important;
      }
    </style>
  ';
}
add_action('admin_head', 'hide_editor_tabs_for_event_description');
